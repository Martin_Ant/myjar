<?php

interface ControllerInterface {

  public function getResponse();

}