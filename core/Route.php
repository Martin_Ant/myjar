<?php

namespace core;


class Route {

  /**
   * Route unique machine name
   *
   * @var  string
   */
  protected $name;

  /**
   * Route URI scheme
   *
   * @var string
   */
  protected $uri;

  /**
   * The requested path
   *
   * @var string
   */
  protected $path;

  /** @var array */
  protected $params;

  /**
   * Info about the controller callable. Array of 2 elements - Path to controller class and method to call
   *
   * @var \ControllerInterface;
   */
  protected $controller;

  /** @var  array */
  protected $definition;

  /**
   * Method to call on controller to get the response
   *
   * @var string $controller_method
   */
  protected $controller_method;

  public function __construct($name, array $params = []) {
    $definition = self::loadRouteDefinition($name);
    $this->definition = $definition;
    $this->name = $name;
    $this->params = $params;
    $this->validateDefinition($definition);
    $this->buildFromDefinition($definition);
  }

  /**
   * Getter for route name
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Getter for route URI scheme
   *
   * @return string
   */
  public function getUri() {
    return $this->uri;
  }

  public function getPath() {
    if (1 === preg_match('/\{[A_zA-Za-z]+\}/', $this->uri)) {
      $uri_bits = explode('/', substr($this->uri, 1, strlen($this->uri)));
      $path_bits = $uri_bits;

      foreach ($uri_bits as $index => $uri_bit) {
        if (1 === preg_match('/\{([A_zA-Za-z]+)\}/', $uri_bit, $matches)) {
          if (isset($this->params[$matches[1]])) {
            $path_bits[$index] = $this->params[$matches[1]];
          }
        }
      }

      $path = implode('/', $path_bits);
      $path = '/' . $path;

      return $path;
    }
    else { // No arguments.
      return $this->uri;
    }
  }

  /**
   * Getter for route paramters
   *
   * @return string
   */
  public function getParameters() {
    return $this->params;
  }

  /**
   * Validate that the definition array contains all the required information.
   *
   * @param array $definition
   * @throws \Exception
   */
  protected function validateDefinition($definition) {
    if (!isset($definition['uri'])) {
      throw new \Exception("Route definitions is missing an URI");
    }

    if (!isset($definition['controller']) || count($definition['controller']) !== 2 ||
      is_callable($definition['controller'][0] . ':' . $definition['controller'][1])) {
      throw new \Exception('Route controller definition invalid or controller not callable.');
    }
  }

  protected function setParam($param_name, $param_value) {
    $this->params[$param_name] = $param_value;
  }

  public static function findRouteMatch($path) {
    global $route_definitions;

    // Strlen to avoid first element being empty string.
    $path_bits = explode('/', substr($path, 1, strlen($path)));
    $parameters = [];

    foreach ($route_definitions as $route_name => $route_definition) {
      $uri = $route_definition['uri'];

      if ($path == $uri) {
        $matching_route = $route_name;
        break;
      }

      $uri_bits = explode('/', substr($uri, 1, strlen($uri)));

      // 0 Is empty string.
      if (count($uri_bits) == count($path_bits) && $uri_bits[0] == $path_bits[0]) {
        // @todo remember that exact match is always better than paramter match
        $match = TRUE;
        foreach ($path_bits as $index => $path_bit) {
          $matching_bit = isset($uri_bits[$index]) && ($path_bit == $uri_bits[$index]);

          if (!$matching_bit) {
            if (1 === preg_match('/\{([A_zA-Za-z]+)\}/', $uri_bits[$index], $matches)) {
              $parameters[$matches[1]] = $path_bit;
            }
            else {
              $match = FALSE;
              break;
            }
          }
        }

        if ($match) {
          $matching_route = $route_name;
        }
      }
    }

    if (isset($matching_route)) {
      return new static($matching_route, $parameters);
    }
  }

  /**
   * Build this route from definition array.
   *
   * @param array $definition
   */
  protected function buildFromDefinition($definition) {
    $this->uri = $definition['uri'];

    $reflection = new \ReflectionClass($definition['controller'][0]);
    $this->controller = $reflection->newInstance();
    $this->controller_method = $definition['controller'][1];
  }

  public function getResponse() {
    // @todo make sure correct paramter positions are beign sent. It's fine when 1 param is expected but there will be
    // problems when parameter cound increases.
    $response = call_user_func_array([
      $this->controller, $this->controller_method,
    ], $this->params);

    if (!$response instanceof Response) {
      throw new \Exception("Route controller returned invalid response");
    }

    return $response;
  }

  public static function loadRouteDefinition($route_name) {
    global $route_definitions;

    if (isset($route_definitions[$route_name])) {
      return $route_definitions[$route_name];
    }
  }
}