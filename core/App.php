<?php

namespace core;

class App {

  /**
   * @var Route $active_route
   */
  protected $active_route;

  public function __construct() {
    try {
      $route = Route::findRouteMatch($_SERVER['REQUEST_URI']);

      if ($route) {
        $this->active_route = $route;

        /** @var Response $response */
        $response = $route->getResponse();
      }
      else {
        $response = new Response(json_encode([
          'message' => 'Resource not found',
        ]), 404);
      }
    }
    catch (\Exception $e) {
      $result = json_encode([
        'message' => $e->getMessage()
      ]);

      $response = new Response($result, 500);
    }

    $response->sendRespone();
  }

  public static function validateEmail($mail) {
    // This is the best that I could come up with myself but this is by far not good enough.
    // /^[A-z0-9]+[A-z]+[A-z0-9]+\@[A-z0-9]+[A-z]+[A-z0-9]+\.[a-z]+$/
    // So in the interest of creating a regex that actually matches emails, the working one is from
    // http://emailregex.com/
    $pattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
    if (1 !== preg_match($pattern, $mail)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * @param $value
   * @param bool $use_salt
   *   Use salt on the value
   * @return string
   */
  public static function encodeValue($value, $use_salt = TRUE){
    global $salt;

    if ($use_salt) {
      return base64_encode($value . $salt);
    }
    else {
      return base64_encode($value);
    }
  }

  /**
   * @param $value
   * @param bool $with_salt
   *   Whether the input value is salted or not.
   * @return string
   */
  public static function decodeValue($value, $with_salt = TRUE){
    global $salt;
    $value = base64_decode($value);

    if ($with_salt) {
      $value = str_replace($salt, '', $value);
    }

    return $value;
  }

}