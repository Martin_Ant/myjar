<?php

namespace core;

class ControllerBase {

  /** @var  \PDO $dbh */
  protected $dbh;

  public function __construct() {
    $this->establishDatabase();
  }

  /**
   * Create database handle.
   */
  protected function establishDatabase() {
    global $debug;

    $dsn = sprintf('%s:dbname=%s; host=%s', DB_TYPE, DB_NAME, DB_HOST);
    $dbh = new \PDO($dsn, DB_USER, DB_PASS);

    if ($debug) {
      $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    $this->dbh = $dbh;
  }

  /**
   * Commit any active transaction.
   */
  public function __destruct() {
    if ($this->dbh->inTransaction()) {
      $this->dbh->commit();
    }
  }
}