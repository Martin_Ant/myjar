<?php

namespace core;

/**
 * @todo we need max allowed items info.
 *
 * Class Pager
 * @package core
 */
class Pager {

  protected $size;

  protected $current_page;

  /**
   * Pager constructor.
   *
   * @param int $size
   * @param int $current_page
   */
  public function __construct($size, $current_page) {
    $this->size = $size;
    $this->current_page = $current_page;
  }

  public function getOffset() {
    return (int) $this->size * $this->current_page - $this->size;
  }

  public function nextPage() {
    $this->current_page += 1;
  }

  public function limit() {
    return (int) $this->size;
  }

  /**
   * @param int $page
   */
  public function setPage($page) {
    $this->current_page = $page;
  }

  public function previousPage() {
    if ($this->current_page > 1) {
      $this->current_page -= 1;
    }
  }
}