<?php

require_once './config.php';

$dsn = sprintf('%s: host=%s;', DB_TYPE, DB_HOST);
$dbh = new \PDO($dsn, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$dbh->exec(
  "CREATE DATABASE IF NOT EXISTS " . DB_NAME . " DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;"
);
