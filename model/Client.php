<?php

namespace model;

use core\App;
use core\Route;

class Client extends Model {

  /**
   * @var int
   */
  protected $id;

  /** @var string */
  protected $email;

  /** @var string */
  protected $name;

  /**
   * @var ArbitraryValue[]
   */
  protected $values;

  /**
   * @return int
   */
  public function id() {
    return $this->id;
  }

  /**
   * @param $email
   */
  public function setEmail($email) {
    $this->email = App::encodeValue($email, TRUE);
  }

  public function getEmail() {
    return App::decodeValue($this->email, TRUE);
  }

  /**
   * @param $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Set arbitrary values for the Client
   *
   * @param array $values
   *   Key value pairs. Key = name, value = value
   */
  public function setValues($values) {
    foreach ($values as $value_name => $value) {
      $arbitrary_value = new ArbitraryValue($this->database_connection, $this, $value_name, $value);
      $this->values[$value_name] = $arbitrary_value;
    }
  }

  /**
   * Decide whether to save or insert the entity
   */
  public function save() {
    if (!$this->id) {
      $this->insert();
    }
    else {
      $this->update();
    }
  }

  /**
   * Load client object from database by email
   *
   * @param \PDO $dbh
   * @param string $email
   * @return static
   */
  public static function loadByMail(\PDO $dbh, $email) {
    $sth = $dbh->prepare(
      'SELECT id, name, email FROM client WHERE email = :email'
    );

    $sth->execute([
      ':email' => App::encodeValue($email, TRUE),
    ]);

    $result = $sth->fetchObject();

    if ($result) {
      $client = new static($dbh);
      $client->id = $result->id;
      $client->email = $result->email;
      $client->setName($result->name);

      return $client;
    }
  }

  /**
   * Load client object from database by id
   *
   * @param \PDO $dbh
   * @param int $id
   * @return static
   */
  public static function load(\PDO $dbh, $id) {
    $sth = $dbh->prepare(
      'SELECT id, name, email FROM client WHERE id = :client_id'
    );

    $sth->execute([
      ':client_id' => $id,
    ]);

    $result = $sth->fetchObject();

    if ($result) {
      $client = new static($dbh);
      $client->id = $result->id;
      $client->email = $result->email;
      $client->setName($result->name);

      return $client;
    }
  }

  /**
   * Insert Client entity to database
   *
   * @throws \Exception
   */
  protected function insert() {
    $this->beginTransaction();

    try {
      // @todo encrypt email
      $entry = [
        ':name' => $this->name,
        ':email' => $this->email,
      ];

      $sth = $this->database_connection->prepare(
        'INSERT INTO client (name, email) VALUES (:name, :email)'
      );

      $sth->execute($entry);

      $client_id = $this->database_connection->lastInsertId();
      $this->id = $client_id;

      if ($this->values) {
        foreach ($this->values as $arbitrary_value) {
          $arbitrary_value->save();
        }
      }
    }
    catch (\Exception $e) {
      $this->rollback();
      throw $e;
    }
  }

  /**
   * Delete entity from database.
   *
   * @throws \Exception
   */
  public function delete() {
    $this->beginTransaction();

    try {
      $entry = [
        ':id' => $this->id,
      ];

      $sth = $this->database_connection->prepare(
        'DELETE FROM client WHERE id = :id '
      );

      $sth->execute($entry);

      if ($this->values) {
        foreach ($this->values as $arbitrary_value) {
          $arbitrary_value->delete();
        }
      }
    }
    catch (\Exception $e) {
      $this->rollback();
      throw $e;
    }
  }

  /**
   * Update existing Client entity in database.
   *
   * @throws \Exception
   */
  protected function update() {
    $this->beginTransaction();

    try {
      $entry = [
        ':name' => $this->name,
        ':email' => $this->email,
        ':id' => $this->id,
      ];

      $sth = $this->database_connection->prepare(
        'UPDATE client SET name = :name, email = :email WHERE id = :id'
      );

      $sth->execute($entry);

      if ($this->values) {
        foreach ($this->values as $arbitrary_value) {
          $arbitrary_value->save();
        }
      }
    }
    catch (\Exception $e) {
      $this->database_connection->rollBack();
      throw $e;
    }
  }

  /**
   * @return ArbitraryValue[]
   */
  public function getValues() {
    if (!$this->values) {

      $sth = $this->database_connection->prepare(
        'SELECT name, value FROM arbitrary_client_values WHERE client_id = :client_id'
      );

      $sth->execute([
        ':client_id' => $this->id,
      ]);

      while ($result = $sth->fetchObject()) {
        $arbitrary_value = new ArbitraryValue($this->database_connection, $this, $result->name, $result->value);
        $this->values[$result->name] = $arbitrary_value;
      }
    }

    return $this->values;
  }

  /**
   * Return JSON representation of this object.
   *
   * @return string
   */
  public function toArray() {
    $result = [
      'id' => $this->id,
      'name' => $this->name,
      'email' => $this->getEmail(),
      'values' => [],
      'links' => [],
    ];

    if ($this->getValues()) {
      foreach ($this->getValues() as $value) {
        $result['values'][$value->getName()] = $value->getValue();
      }
    }

    // @todo This should probably be independent from the Client model.
    $route = new Route('clients.edit', ['client_id' => $this->id]);

    if ($route) {
      $result['links']['edit'] = $route->getPath();
    }

    return $result;
  }
}