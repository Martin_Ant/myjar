<?php

namespace model;

class ArbitraryValue extends Model {

  /** @var  Client */
  protected $client;

  /** @var string */
  protected $name;

  /** @var string */
  protected $value;

  /** @var  \PDO $database_connection */
  protected $database_connection;

  public function __construct($database_connetion, Client $client, $name, $value = NULL) {
    parent::__construct($database_connetion);

    $this->client = $client;
    $this->name = $name;

    if (isset($value)) {
      $this->setValue($value);
    }
  }

  /**
   * @return mixed
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return mixed
   */
  public function getValue() {
    if (!$this->value) {
      $this->loadValue();
    }

    return $this->value;
  }

  /**
   * @param mixed $value
   */
  public function setValue($value) {
    $this->value = $value;
  }

  /**
   * Load the value associated with Client ID and name.
   */
  protected function loadValue() {
    $sth = $this->database_connection->prepare(
      'SELECT value FROM arbitrary_client_values WHERE client_id = :client_id AND name = :name'
    );

    $sth->execute([
      ':client_id' => $this->client->id(),
      ':name' => $this->name,
    ]);

    $value = $sth->fetchColumn();

    return $value;
  }

  /**
   * Insert arbitrary value in database.
   *
   * @throws \Exception
   */
  protected function insert() {
    $this->beginTransaction();

    if (!$this->client->id()) {
      throw new \Exception('Client has to be saved before arbitrary value can be saved');
    }

    try {
      $entry = [
        ':client_id' => $this->client->id(),
        ':name' => $this->name,
        ':value' => $this->value,
      ];

      $sth = $this->database_connection->prepare(
        'INSERT INTO arbitrary_client_values (name, client_id, value) VALUES (:name, :client_id, :value)'
      );

      $sth->execute($entry);
    }
    catch (\Exception $e) {
      $this->rollBack();
      throw $e;
    }
  }

  /**
   * Delete entity from database.
   *
   * @throws \Exception
   */
  public function delete() {
    $this->beginTransaction();

    if (!$this->client->id()) {
      throw new \Exception('Client has to be saved before arbitrary value can be saved');
    }

    try {
      $entry = [
        ':client_id' => $this->client->id(),
        ':name' => $this->name,
      ];

      $sth = $this->database_connection->prepare(
        'DELETE FROM arbitrary_client_values WHERE name = :name AND client_id = :client_id)'
      );

      $sth->execute($entry);
    }
    catch (\Exception $e) {
      $this->rollBack();
      throw $e;
    }
  }

  /**
   * Update the arbitrary entity value in database.
   *
   * @return bool
   * @throws \Exception
   */
  protected function update() {
    $this->beginTransaction();

    try {
      $sth = $this->database_connection->prepare(
        'UPDATE arbitrary_client_values SET value = :value WHERE client_id = :client_id AND name = :name'
      );

      return $sth->execute([
        ':client_id' => $this->client->id(),
        ':name' => $this->name,
        ':value' => $this->value,
      ]);
    }
    catch (\Exception $e) {
      $this->rollback();
      throw $e;
    }
  }

  /**
   * Choose to insert or update the entity.
   */
  public function save() {
    // No existing entry
    if (!$this->loadValue()) {
      $this->insert();
    }
    else {
      $this->update();
    }
  }

}