<?php

namespace model;

class Model {

  /**
   * @var \PDO $database_connection
   */
  protected $database_connection;

  public function __construct(\PDO $database_connection) {
    $this->database_connection = $database_connection;
  }

  /**
   * Rollback active database transaction.
   */
  public function rollback() {
    if ($this->database_connection->inTransaction()) {
      $this->database_connection->rollBack();
    }
  }

  /**
   * Begin database transaction if none is active.
   */
  public function beginTransaction() {
    if (!$this->database_connection->inTransaction()) {
      $this->database_connection->beginTransaction();
    }
  }
}