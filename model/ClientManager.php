<?php

namespace model;

use core\Pager;

class ClientManager extends Model {

  public function listAllClients(Pager $pager) {
    /** @var Client[] $clients */
    $clients = [];

    $sth = $this->database_connection->prepare(
      "SELECT id FROM client LIMIT :limit OFFSET :offset"
    );

    $sth->bindValue(':limit', $pager->limit(), \PDO::PARAM_INT);
    $sth->bindValue(':offset', $pager->getOffset(), \PDO::PARAM_INT);
    $sth->execute();

    while ($result = $sth->fetchObject()) {
      $client = Client::load($this->database_connection, $result->id);
      $clients[$client->id()] = $client;
    }

    return $clients;
  }
}