<?php

define('DB_TYPE', 'mysql');
define('DB_NAME', 'dev');
define('DB_HOST', 'mysql');
define('DB_USER', 'container');
define('DB_PASS', 'container');

// Debug mode.
$GLOBALS['debug'] = TRUE;

// Salt to encode values with
$GLOBALS['salt'] = '|IY3V&~|z)J,X$gtv<C$(+dm O3 .$8-APCW,8 F`AJO[@S=Ta$-$/WDI!@@2_Ue';