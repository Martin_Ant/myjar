<?php

namespace controller;
use core\App;
use core\ControllerBase;
use core\Pager;
use core\Response;
use model\Client;
use model\ClientManager;

class ClientController extends ControllerBase {

  /**
   * Create the Client entity
   *
   * @return Response
   */
  public function addClient() {
    try {
      $this->validateClientInsert();

      $values = $_POST;
      unset($values['name']);
      unset($values['email']);

      $client = new Client($this->dbh);
      $client->setEmail($_POST['email']);
      $client->setName($_POST['name']);
      $client->setValues($values);
      $client->save();

      return new Response('', 201);
    }
    catch (\Exception $e) {
      $result = json_encode([
        'message' => $e->getMessage()
      ]);

      return new Response($result, 400);
    }
  }

  /**
   * Validate the POST data for client insertion
   *
   * @throws \Exception
   *   Throws exception for every validation error.
   */
  public function validateClientInsert() {
    if (empty($_POST)) {
      throw new \Exception('Empty payload');
    }

    if (count($_POST) > 10) {
      throw new \Exception('Maximum payload size is 10');
    }

    if (!isset($_POST['email'])) {
      throw new \Exception('Field \'email\' is required field');
    }

    if (!isset($_POST['name'])) {
      throw new \Exception('Field \'name\' is required field');
    }

    if (!App::validateEmail($_POST['email'])) {
      throw new \Exception('Invalid email address');
    }

    if (Client::loadByMail($this->dbh, $_POST['email'])) {
      throw new \Exception('Email already in use');
    }
  }

  public function listClients() {
    $client_manager = new ClientManager($this->dbh);
    $current_page = 1;
    $pager = new Pager(10, $current_page);
    $clients = $client_manager->listAllClients($pager);
    $list = [];

    foreach ($clients as $client) {
      $list[] = $client->toArray();
    }

    $json = json_encode($list);

    return new Response($json);
  }

  public function validateClientEdit() {
    if (empty($_POST)) {
      throw new \Exception('Empty payload');
    }

    if (count($_POST) > 10) {
      throw new \Exception('Maximum payload size is 10');
    }

    if (isset($_POST['email'])) {
      if (!App::validateEmail($_POST['email'])) {
        throw new \Exception('Invalid email address');
      }
    }
  }

  /**
   * @param $client_id
   * @return Response
   */
  public function editClient($client_id) {
    $this->validateClientEdit();
    $client = Client::load($this->dbh, $client_id);

    if (!$client) {
      $result = json_encode([
        'message' => 'Client does not exist;,'
      ]);

      return new Response($result, 400);
    }

    $values = $_POST;

    if (isset($values['email'])) {
      $client->setEmail($values['email']);
      unset($values['email']);
    }

    if (isset($values['name'])) {
      $client->setName($values['name']);
      unset($values['name']);
    }

    $client->setValues($values);

    $client->save();

    // Just send 200
    return new Response('');
  }

}