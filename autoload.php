<?php

spl_autoload_register(function ($class_name) {
  $filename = str_replace("\\", '/', $class_name) . ".php";
  require_once $filename;
});