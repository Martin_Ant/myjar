<?php

use core\App;

require_once './autoload.php';
require_once './config.php';
require_once './routes.php';

new App();