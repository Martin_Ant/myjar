Preface:
I remembered the requirements wrong and instead of making the phone and email required field I have done name and email.
Also the email field is encrypted in database instead of the non existing phone field.

Half of the requirements are unfulfilled but this is the most I could do with the time I had. If this is not enough, then
sorry but I don't have any more free time to invest in this.

This is built using dockerizedrupal.com docker containers and has not been tested on a regular LAMP setup (Well in essence
it is 'regular' LAMP setup). However there is nothing exotic here and this should work if you have the following:
  - PHP 7
  - Mysql 5
  - Apache 2.4

Installation:
Step 1
Import db.sql into your database.

Step 2
Change the config.php to match your database credentials.

Available endpoints

/clients
  Will list all known clients. With pager (not implemented)
/clients/add
  Will accept POST data and created new client entity. Required keys are 'email' and 'name'
/clients/edit/{client_id} where {client_id} is numeric value
  Will change existing client. Atleast 1 payload element required. If not 'email' or 'name' then its assumed to be arbitrary value
  and will be saved to database with client.

Every endpoint will return JSON. Even when there is an error on server side, the App will show the error as JSON and will send
the response with the most accurate Http response code - nothing fancy, just 201, 200, 400, 500