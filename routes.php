<?php

$GLOBALS['route_definitions'] = [
  'clients.add' => [
    'uri' => '/clients/add',
    'accepts' => [
      'POST',
    ],
    'controller' => [
      'controller\ClientController', 'addClient'
    ],
  ],
  'clients.list' => [
    'uri' => '/clients',
    'accepts' => [
      'POST',
    ],
    'controller' => [
      'controller\ClientController', 'listClients'
    ],
  ],
  'clients.edit' => [
    'uri' => '/clients/edit/{client_id}', // Arguments can only contain characters. Uri scheme cant start with a parameter
    'accepts' => [
      'POST',
    ],
    'controller' => [
      'controller\ClientController', 'editClient'
    ],
  ],
];